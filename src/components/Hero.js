import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {dayAsString, simpleDate} from '../tools/DateTools';

const Hero = ({selectedArea, date}) => {
    const areaSelected = !_.isEmpty(selectedArea);
    return (
        <div className="hero is-link is-bold">
            <div className="hero-head has-text-right">
            </div>
                    <div className="hero-body">
                        <p className="title herotitle">{areaSelected ? selectedArea.longName : "Dagen i dag"}</p>
                        <p className="subtitle herotitle">{dayAsString(date.getDay())} - {simpleDate(date)}</p>
                    </div>
                    <div className="hero-foot">
                       
                    </div>
            </div>
    )
}

// <progress max="100" value="50" className="dailyProgress progress is-warning">test</progress>

export default Hero;