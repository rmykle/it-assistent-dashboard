import React from 'react';

export default ({time, elapsed}) => {
    return (
        <div className="clock">
        <p>{time.toLocaleTimeString()}</p>
            <div className="clockProgress" style={{width:`${elapsed}%`}}>
            </div>
        </div>
    )
}