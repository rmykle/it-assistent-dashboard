import React from 'react';

export default ({text}) => {
    return (
        <h3 className="title is-3 containerTitle">
            {text}
        </h3>
    )
}