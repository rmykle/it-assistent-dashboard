import React from 'react';
import _ from 'lodash';
import Title from './Title';


const Selection = ({areas, onAreaSelect}) => {
    return (     
            <div className="container">
                <Title text="Område"/>
                        { _.toArray(areas).map(area => {
                            return <AreaBox area={area} onClick={onAreaSelect} key={area.id} />  
                        })
                        }
            </div>
    )
}

const AreaBox = ({area, onClick}) => {
    return (
            <div className="box areabox" onClick={() => onClick(area)}>
                <div className="content has-text-centered">
                    <p className="title">{area.shortName}</p>
                    <p className="subtitle">{area.longName}</p>
                </div>
            </div>
    )
}

export default Selection;
