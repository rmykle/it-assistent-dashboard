import React, { Component } from 'react';
import { createIcon } from '../tools/Icons';

class RoomFilter extends Component {

    constructor(props) {
        super(props);
        const options = [{title: "Alle", icon:"globe"}, {title: "Stengerunde", icon:"lock"}];
        this.state = {options: options, category: options[0], term:"", hideEmpty:false}

    }

    render() {
        return (
            <div className="level">
                {this.tabs()}
                {this.searchFilters()}
            </div>
        )
    }

    tabs() {
        return(
            <div className="level-left">
                <div className="level-item tabs is-medium">
                    <ul>
                        {this.state.options.map(option => {
                            return this.tab(option);
                        })}
                    </ul>
                </div>
            </div>
        )
    }

    tab(option) {
        return (
            <li 
            key={option.title}
            className={this.state.category == option ? "is-active" : ""}>
                <a 
                href="#"
                onClick={(event) => this.categorySelected(event, option)}   >
                {createIcon(option.icon, null, "smallIcon")}
                {option.title}
                </a>
            </li>
        )
    }
    

    categorySelected(event, option) {
        event.preventDefault();
        this.setState({category: option}, this.updateFilters);
    }

    searchFilters() {
        return(
            <div className="level-right">
                <div className="level-item extraFilters">   
                    {this.emptyCheck()}    
                    {this.searchInput()}
                </div>
            </div>
        )
    }

    emptyCheck() {
        return (
            <label className="checkbox">
                <input type="checkbox" checked={this.state.hideEmpty} onClick={(event) => {
                    this.toggleEmpty();
                }}/>
                Kun rom med undervisning
            </label>
        )
    }

    toggleEmpty(){
        this.setState({hideEmpty: !this.state.hideEmpty}, this.updateFilters);
    }

    searchInput() {
        return(
            <div className="control">
                        <input className="input"
                        placeholder="Romnavn"
                        value={this.state.term}
                        onChange={(e) => {
                            this.searchTermChanged(e.target.value)}
                        }
                        />
                    </div>
        )
    }
    searchTermChanged(term) {
        this.setState({term: term}, this.updateFilters);
    }

    updateFilters() {
        this.props.updateFilters(this.state);
    }

}
export default RoomFilter;