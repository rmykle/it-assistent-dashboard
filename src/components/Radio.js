import React, { Component } from 'react';
import { createIcon } from '../tools/Icons';

export default class Radio extends Component {
    constructor(props) {
        super(props);
        this.state = {on: false, isPlaying: false, selectedChannel: "placeholder", volume: 5} 
        this.toggleRadio = this.toggleRadio.bind(this);
        this.togglePlay = this.togglePlay.bind(this);
    }
    render() {
        const powerClasses = this.state.on ? "radioPower radioPowerOn" : "radioPower";
        const {selectedChannel} = this.state;
        return (
            <div className="radioPlayer">
                <audio ref="radio">
                    <source src={selectedChannel != "placeholder" ?this.state.selectedChannel : ""} type="audio/mpeg"/>
                </audio>
                {false ? this.channelSelector() : <i><strong>RADIO</strong></i>}
                {this.controls()} 
                <a href="#">{createIcon("power-off", this.toggleRadio, powerClasses)}</a>
            </div>

        )
    }


    channelSelector() {
        return (
            <div className="select is-warning">
                <select value={this.state.selectedChannel} onChange={(event) => this.selectChannel(event)}>
                    <option value="placeholder">-- Velg kanal --</option>
                    <option value="http://lyd.nrk.no/nrk_radio_p1_ostlandssendingen_mp3_h">NRK P1</option>
                    <option value="http://lyd.nrk.no/nrk_radio_p3_mp3_h">NRK P3</option>
                    <option value="http://lyd.nrk.no/nrk_radio_p1pluss_mp3_h.m3u">NRK P1+</option>
                    <option value="http://lyd.nrk.no/nrk_radio_p13_mp3_h">NRK P13</option>
                    <option value="http://lyd.nrk.no/nrk_radio_mp3_mp3_h">NRK mP3</option>
                    <option value="http://lyd.nrk.no/nrk_radio_p13_mp3_h">P13</option>
                    <option value="http://stream.p4.no/p4_mp3_hq">P4</option>
                    <option value="http://stream.p4.no/p5bergen_mp3_hq">P5 Hits Bergen</option>
                    <option value="http://stream.p4.no/p6_mp3_hq">P6 Rock</option>
                </select>
            </div>
        )
    }

    selectChannel(event) {
        const {value} = event.target;
        if(value != this.state.selectedChannel) {
            this.setState({selectedChannel: value}, () => {
                const {radio} = this.refs;
            radio.load();
            this.play();
        })
    }
    }

    play() {
        this.setState({isPlaying: true});
        this.refs.radio.play();
    }

    controls() {

        const playText = this.state.isPlaying ? "pause" : "play";
        return (
            <div className={this.state.on ? "radioControls radioOn" : "radioControls radioOff "}>
            {this.channelSelector()}
            {createIcon(playText, this.togglePlay, "smallIcon")}
            {createIcon("volume-off", () => {
            }, "smallIcon")}
            <input type="range" className="slider" value={this.state.volume} max="10" onChange={(e) => this.volumeChange(e)}/>
            </div>
        )
    }

    volumeChange(event) {
        this.setState({volume: event.target.value}, () => {
            this.refs.radio.volume = this.state.volume / 10;
        });
    }

    toggleRadio() {
        this.setState({on: !this.state.on});
    }

    togglePlay() {
        this.setState({isPlaying: !this.state.isPlaying}, () => {
            if(!this.state.isPlaying){

                this.refs.radio.pause();
            }
            else if(this.state.selectedChannel != "placeholder") {
                this.refs.radio.play();
            }
        })
    }

}