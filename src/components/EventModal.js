import React from 'react';

export default ({event, selectEvent}) => {
    if(!event){
        return (
            ""
        )
    }
    return (
        <div className={event ? "modal is-active" : "modal"}>
            <div className="modal-background" onClick={() => selectEvent()}></div>
            <div className="modal-content">
                <div className="box content">
                    <div className="card">
                        <div className="card-header level is-marginless">
                            <div className="level-left">
                                <p className="level-item card-header-title">
                                    {`${event.courseid} - ${event.type}`}
                                </p>
                            </div>
                            <div className="level-right">
                                <p className="level-item">
                                    {`${event.startString} - ${event.endString}`}
                                </p>
                            </div>
                        </div>
                        <div className="card-content">
                            <i>{event.staffnames.map(name => `${name} `)}</i>
                            <p>{event.summary}</p>
                        </div>
                    </div>
                </div>
            </div>
            <button className="modal-close is-large" onClick={() => eventSelected()}></button> 
        </div>
    )
}