import React, { Component } from 'react';
import Title from './Title';
import Loader from './Loader';
import RoomFilter from './RoomFilter';
import _ from 'lodash';
import RoomTable from '../containers/RoomTable';
import {connect} from 'react-redux';
import { createIcon } from '../tools/Icons';

class RoomList extends Component{
    constructor(props) {
        super(props);
        this.state = {filters: {}}
        this.updateFilters = this.updateFilters.bind(this);
        this.filterRoom = this.filterRoom.bind(this);
        
    }
    render() {

        if(!this.props.selectedArea.buildings) {
            return (
                <div className="container">
                    <Title text="Rom"/>
                    <Loader/>
                </div>
            )
        }
        return (
            <div className="container">
                <Title text="Rom"/>
                <RoomFilter updateFilters={this.updateFilters}/>

                {this.roomList()}
            </div>
        )
    }

    roomList() {
        
        return (
            <div className="box">
                <ul className="roomTables">
                    {this.sortBuildings(this.props.selectedArea.buildings)
                        .map(building => {
                            return building.rooms.filter(this.filterRoom)
                            .map(room => <RoomTable room={room} key={room.id} elapsed={this.props.elapsed}/>);
                        })}
                </ul>
            </div>
        )
    }

    filterRoom(room) {
        if(this.filterByCategory(room)) return false;
        if(this.filterByEmpty(room)) return false;
        if(this.filterByName(room)) return false;
        return true;
    }

    filterByCategory(room) {
        const {category} = this.state.filters;
        if(!category || category.title === "Alle") return false;
        else if(room.check && room.check !== this.props.selectedDate.getDay()) return true;
        return false;
    }

    filterByEmpty(room){
        const {hideEmpty} = this.state.filters;
        return hideEmpty && room.events.length === 0;
    }

    filterByName(room) {
        const {term} = this.state.filters;
        return term && !room.name.toLowerCase().includes(term.toLowerCase()); 
    }
    
    sortBuildings(buildings) {
        return _.toArray(buildings).sort((a,b) => {
            return b.rooms.length - a.rooms.length;
        })
    }

    updateFilters(filters) {
        this.setState({filters: filters});
    }
    
}

export default connect(null, null)(RoomList);