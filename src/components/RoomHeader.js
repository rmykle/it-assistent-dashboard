import React from 'react';
import {createIcon} from '../tools/Icons';

export default ({room, selectedDay}) => {
    return (
        <div className={`card-header`}>
            <p className="card-header-title">
                {room.name}
            </p>
            {headerIcons(room, selectedDay.getDay())}
        </div>  
    )
}

const headerIcons = (room, selectedDay)=> {
    const checkToday = selectedDay < 6 && (!room.hasOwnProperty("check") || (room.check && room.check === selectedDay));
    const wellToday = checkToday && (room.well === selectedDay || room.check === selectedDay);
    return (
        <p className="card-header-icon">
                {checkRoomToday(room, selectedDay) ? lock(room, selectedDay) : ""}
        </p>
    )
    
}

const checkRoomToday = (room, day) => {
    return day < 6 && (!room.hasOwnProperty("check") || (room.check && room.check === day));
}

const lock = (room, day) => {
    return (
        <span className="icon smallIcon">
                <i className="fas fa-lock">
                    {wellToday(room, day) ? "B" : ""}
                </i>
        </span>
    )
}

const wellToday = (room, day) => {
    return room.well === day || room.check === day;
}