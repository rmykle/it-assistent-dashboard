import React from 'react';
import {selectArea} from '../actions';
import Radio from '../components/Radio';

const Menu = ({onAreaSelect}) => {
    
    return (
        <nav>
            <a className="logo is-3" href="#" onClick={() => onAreaSelect()}>
                <span className="icon is-large">
                    <i className="fas fa-laptop"></i>
                </span>
                <h2 className="title is-2">IT-ass</h2>
            </a>

                <div>
                    <Radio/>
                </div>
        </nav>
    )
    
}

export default Menu;
