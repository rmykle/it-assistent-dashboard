import _ from 'lodash';


export const simpleString = date => {
    const hour = date.getHours();
    const minute = date.getMinutes();
    return (hour < 10 ? `0${hour}` : hour) + ":" + (minute < 10 ? `0${minute}` : minute);
}

export const currentDate = () => {
    return new Date().toISOString().split('T')[0];
}

export const formatDispute = dispute => {
    return {...dispute, DTSTART: convertCalenderDate(dispute.DTSTART), DTEND: convertCalenderDate(dispute.DTEND)};
}
const convertCalenderDate = (time) => {
    const year = time.substring(0,4);
    const month = time.substring(4,6);
    const day = time.substring(6,8);
    const hour = time.substring(9, 11);
    const minute = time.substring(11, 13);
    return new Date(Date.UTC(year, month, day, hour, minute));
}

export const formatDateEvent = data => {
    return _.mapValues(data, building=> {
        return {...building, rooms: building.rooms.map(room => {
            return {...room, events: room.events.map(event => {
              
                const start = new Date(event.start);
                const end = new Date(event.end);
                
                return {...event, start: start, end: end,
                startString: simpleString(start), endString: simpleString(end)}
            })}
        })
    }}
)}

export const formatValidEventDates = date => {
    const time = date.split(":");
    let minute = parseInt(time[1]);
    let edited = false;
    while(minute % 15 != 0) {
        minute--;
        edited = true;
    }
    return edited ? time[0]+":"+minute : date;
}

export const dayAsString = day => {
    switch(day) {
        case 1: return "Mandag";
        case 2: return "Tirsdag";
        case 3: return "Onsdag";
        case 4: return "Torsdag";
        case 5: return "Fredag";
        case 6: return "Lørdag";
        case 6: return "Søndag";
        default: return "";
    }
}

export const simpleDate = date => {
    const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const month = date.getMonth() < 10 ? `0${date.getMonth()+1}` : date.getMonth()+1;
    const year = date.getFullYear();
    return day + "."+month+"."+year;
}

