import React from 'react';
export const createIcon = (text, onClick, classes) => {
    return (
        <span className={"icon " + classes} onClick={onClick}>
            <i className={`fas fa-${text}`}></i>
        </span>
    )
}