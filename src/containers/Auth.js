import React, { Component } from 'react';
import Title from '../components/Title';
import {startAuth} from '../actions';
import {connect} from 'react-redux';

class Auth extends Component {
    constructor(props){
        super(props);
        this.state = {input:""}
    }

    componentDidMount(){
        this.input.focus();
    }

    componentDidUpdate(){
        this.input.focus();
    }

    render() {
        const {isFailure, isInitated} = this.props;
        const color = isFailure ? "is-danger" : "is-link";
        return (
            <div>
                <Title text="PIN"/>
                <form className="pinForm">
                    <input 
                    ref={(input) => {this.input = input;}}
                    disabled={isInitated}
                    autoComplete="off" 
                    onChange={(e) => this.onInputChange(e.target.value)}
                    className={`input ${color}`} 
                    type="password" 
                    value={this.state.input}
                    />
                    {isFailure && this.state.input.length == 0 ? <p>Ugyldig PIN</p> : null} 
                </form>
                
            </div>
        )
    }
    onInputChange(input) {
        if(this.props.isInitated || !/^\s*|\d+$/.test(input)){
            return;
        }
        else if(input.length >= 4){
            this.props.startAuth(input);
            this.setState({input:""})
        }
        else {
            this.setState({input: input});
        }
    }
}

export default connect((state) => state.auth, {startAuth})(Auth);