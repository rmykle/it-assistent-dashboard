import React, { Component } from 'react';
import {simpleDate, simpleString, formatValidEventDates} from '../tools/DateTools'
import {connect} from 'react-redux';
import {fetchRoomData, selectEvent} from '../actions/';
import Loader from '../components/Loader';
import RoomHeader from '../components/RoomHeader';

const TIME_TABLE ="Timeplan";
const INFORMATION = "Informasjon";

class RoomTable extends Component {

    constructor(props){
        super(props);
        this.state = {selectedInfo: TIME_TABLE};
    }

    render() {
        return (
            <li className="card">
                {<RoomHeader room={this.props.room} selectedDay={this.props.selectedDay}    />}
                <div className="card-table">
                {this.state.selectedInfo === INFORMATION ? this.information() : this.timeTable()}
                </div>
                <div className="card-footer">
                    {this.menuItem(TIME_TABLE)}
                    {this.menuItem(INFORMATION)}
                </div>
            </li>
        );
    }

    

    menuItem(text) {
        return (
            <a href="#" className={this.state.selectedInfo === text ? "card-footer-item selectedRoomElement" : "card-footer-item"} onClick={(e) => {
                e.preventDefault();
                this.setState({selectedInfo: text});
                if(!this.props.rooms[this.props.room.id] && text === INFORMATION){
                    this.props.fetchRoomData(this.props.room.id);
                }
            }}>{<span className="icon smallIcon">
            <i className={text===INFORMATION ? "fas fa-info":"far  fa-calendar-alt"}></i>
        </span>}</a>
        )
    }

    timeTable() {

        return (
            <div className="tableContainer">
            {this.props.room.events.length === 0 ?
                 <div className="tableOverlay">TOM DAG</div> :
                 <div className="timeOverlay">
                    <div style={{height:`${this.props.elapsed}%`}}></div>
                    <div className="timeBar"></div>
                 </div> }
            <ul className="timeTable">
                {this.listElements()}
            </ul>
            </div>
        )
    }

    listElements() {
        const elements = [];
        let date = new Date();

        date.setHours(8);
        date.setMinutes(0);
        date.setSeconds(0);

        while(date.getHours() < 18){
            const hourString = simpleString(date);
            elements.push(
            <li 
            key={hourString} 
            time={hourString}
            className={date.getHours() % 2 === 1 ? "oddHour" : ""}
            >
            {this.props.room.events.filter(roomEvent => formatValidEventDates(roomEvent.startString) === hourString).map((roomEvent) => {
                return this.createEvent(roomEvent);
            })}
            </li>);
            date.setMinutes(date.getMinutes() + 15);
            
        }
        return elements;
    }


    information() {
        const id = this.props.room.id;

        const roomData = this.props.rooms[id];
        if(roomData) {
            if(roomData.failed){
                return "ERROR";
            }
            else if(roomData.data) {
                return this.infoList();
            }
            
        }
            return <Loader/>

    }

    infoList() {
        const roomInfo = this.props.rooms[this.props.room.id];
        const {data, equipment} = roomInfo;
        return (
            <ul className="roomInfoList">
                {data.floornum ? <li>{`${data.floornum} etasje`}</li>: ""}
                {data.size ? <li>{`${data.size} plasser`}</li>: ""}
                <li><a target="_blank" href={`http://use.mazemap.com/?v=1&campuses=uib&sharepoitype=identifier&sharepoi=${data.buildingid}:${data.roomid}`}>MazeMap</a></li>
                <li><ul>{equipment.map(eq => {return <li key={eq}>{eq}</li>})}</ul></li>
            </ul>
        )
    }



    createEvent(event) {
        const quarters = (event.end.getTime() - event.start.getTime()) / 60000 / 15;
        
        return (
            <div 
            key={event.startString} 
            className="event"
            style={{height: (quarters * 7)}}
            onClick={() => this.props.selectEvent(event)}
            >
                <p>{event.type}<br/>{`${event.startString} - ${event.endString}`}</p>
            </div>
        )
    }

}

export default connect(({rooms, selections}) => ({rooms: rooms, selectedDay: selections.selectedDate}) 
, {fetchRoomData, selectEvent})(RoomTable);