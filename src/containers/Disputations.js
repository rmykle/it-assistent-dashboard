import React, { Component } from 'react';
import {connect} from 'react-redux';
import Title from '../components/Title';
import {fetchDisputations} from '../actions';
import _ from 'lodash';
import {dayAsString, simpleDate, simpleString} from '../tools/DateTools';
import Loader from '../components/Loader';

class Disputations extends Component {
    componentDidMount() {
        this.props.fetchDisputations();
    }
    render() {
        if(!this.props.disputations) {
            return (
                <div className="container">
                    <Title text="Disputeringer"/>
                    <Loader/>
                </div>
            )
        }
        return (
            <div className="container">
                <Title text="Disputeringer" />
                <ul className="disputeList">{this.disputeList()}</ul>
            </div>
        )
    }

    disputeList() {
        const disputations = _.groupBy(this.props.disputations, (dispute) => {
            const date = dispute.DTSTART;
            return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        });

        const sortedKeys =  Object.keys(disputations).sort((a,b) => {
            return new Date(a) - new Date(b);
        })
        
        return sortedKeys.map(dateKey => {
            return this.disputeDay(dateKey, disputations[dateKey]);
        })
    }
    disputeDay(key, disputes) {
        const date = new Date(key);
        let nextID = 0;
        return (
        <li key={key} className="box">
            <p className="title is-4">{dayAsString(date.getDay()) + " " + simpleDate(date)} </p>  
            <ul className="dailyDisputes">
                {disputes.map(dispute => {
                    return this.dispute(dispute, nextID++);
                })}
            </ul>
        </li>
        )
    }

    dispute(dispute, id){
        return(
            <li key={id} className="dispute">
                <a href={dispute.URL} target="_blank" className="is-5"><strong>{dispute.SUMMARY}</strong></a>
                <p>
                <strong>{simpleString(dispute.DTSTART) + " - " + simpleString(dispute.DTEND) + " | "}</strong>{dispute.LOCATION}
                </p>
                
               
            </li>
        )
    }

    

}

export default connect((state) => ({disputations: state.disputations}), {fetchDisputations})(Disputations);
