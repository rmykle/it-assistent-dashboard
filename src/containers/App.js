import React, { Component } from 'react';
import Menu from '../components/Menu';
import Disputations from '../containers/Disputations';
import Hero from '../components/Hero';
import {connect} from 'react-redux';
import {selectArea, selectEvent} from '../actions';
import EventModal from '../components/EventModal';
import Selection from '../components/Selection';
import RoomList from '../components/RoomList';
import Clock from '../components/Clock';
import Auth from './Auth';


class App extends Component{
    constructor(props){
        super(props);
        this.onAreaSelect = this.onAreaSelect.bind(this);
        const time = new Date();
        const start = new Date(time.getFullYear(), time.getMonth(), time.getDate(), 8);
        const end = new Date(time.getFullYear(), time.getMonth(), time.getDate(), 18);
        const totalTime = end-start;
        this.state = {time: time, start:start, end:end, elapsed: 0}
    }
    componentDidMount() {
        this.timeID = setInterval(
            () => this.tick(), 1000
        );
    }

    componentWillUnmount(){
        clearInterval(this.timeID);
    }

    render(){
        const {selectedEvent,selectedArea, selectedDate} = this.props.selections;
        const selected = this.props.areas[selectedArea];

        return (
            <div>
                <Menu onAreaSelect={this.onAreaSelect}/>
                <Hero selectedArea={selected} date={this.state.time}/>
                {this.props.auth.isAuthed ? this.mainContent(selected, selectedDate): <Auth/>}
                <EventModal event={this.props.selections.selectedEvent} selectEvent={this.props.selectEvent} />
            </div>
        )

    }

    mainContent(selected, selectedDate) {
 
        if(selected) {
            return (
                <div>
                    <Clock time={this.state.time} elapsed={this.state.elapsed}/>
                    <RoomList selectedArea={selected} selectedDate={selectedDate} elapsed={this.state.elapsed}/>
                    <Disputations />
                </div>
            )
        }
        else return (
            <div>
                <Selection areas={this.props.areas} onAreaSelect={this.onAreaSelect}/>
                <Disputations/>
            </div>
        )
    }

    onAreaSelect(selected) {
        this.props.selectArea(selected);
    }

    tick(){
        const time = new Date();
        
        const elapsedTime = time-this.state.start;
        const elapsed = Math.round(elapsedTime/(this.state.end-this.state.start)*100);

        this.setState({time: new Date(), elapsed: elapsed <100? elapsed : 100});
    }
    
}

export default connect(state => ({selections: state.selections, areas: state.areas, auth:state.auth}), 
{selectArea, selectEvent})(App);