export const SELECTED_AREA = "SELECTED_AREA";
export const SELECTED_EVENT = "SELECTED_EVENT";
export const FETCH_ROOM_DATA = "FETCH_ROOM_DATA";
export const FETCH_ROOM_DATA_INITIATED = "FETCH_ROOM_DATA_INITIATED";
export const FETCH_ROOM_DATA_FAILED = "FETCH_ROOM_DATA_FAILED";
export const GET_AREA_DETAILS = "GET_AREA_DETAILS";
export const FETCH_DISPUTES = "FETCH_DISPUTES";

export const AUTH_INITIATE = "AUTH_INITIATE";
export const AUTH_FAILED ="AUTH_FAILED";
export const AUTH_SUCCESS = "AUTH_SUCCESS";

import axios from 'axios';
import {formatDateEvent, currentDate, formatDispute} from '../tools/DateTools';

const developMode = false;


export const selectArea = selected => {

    if(!selected) {
        return {
            type: SELECTED_AREA,
            payload: undefined
        }
    }

    const url = developMode ? `/mock/${selected.id}.json` : `/scripts/schedule.php?id=${selected.id}&day=${currentDate()}`;
    const request = axios.get(url);
    return(dispatch)=> {
        dispatch({type:SELECTED_AREA, payload: selected.id});
        
        if(!selected.buildings){
            request.then(({data}) => {
                
            dispatch({type:GET_AREA_DETAILS, payload: {...selected, buildings: formatDateEvent(data)}});
            }).catch(error => {
                
            });
        }
    }
}

export const selectEvent = eventSelected => {
    return {
        type: SELECTED_EVENT,
        payload: eventSelected
    }
}

export const fetchRoomData = roomid => {
    return (dispatch) => {
        dispatch({type: FETCH_ROOM_DATA_INITIATED, payload: roomid})
        const url = developMode ? "/mock/5954601.json" : `/scripts/room.php?id=${roomid}`;
        const request = axios.get(url);

        request.then(({data}) => {
            dispatch({type: FETCH_ROOM_DATA, payload: data})
        }).
        catch(error => {
            dispatch({type: FETCH_ROOM_DATA_FAILED, payload: roomid})
        })
    }
    
}

export const fetchDisputations = () => {
    return (dispatch) => {
        const url = developMode ? "/mock/disput.json"  : "/scripts/disput.php";
        const request = axios.get(url);
        request.then(({data}) => {
            dispatch({type: FETCH_DISPUTES, payload: data.map(dispute => {
                return formatDispute(dispute);
            })})
        })
    }
}

export const startAuth = (user) => {
    return dispatch => {
        dispatch({type:AUTH_INITIATE});
        setTimeout(() => { //todo serverside
            if(user==1234){
                dispatch({type: AUTH_SUCCESS})
            }
            else dispatch({type: AUTH_FAILED})
        }, 500)
    }
}


