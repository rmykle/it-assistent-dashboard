import {combineReducers} from 'redux';
import areas from './area_reducer';
import selections from'./selected_reducer';
import rooms from './room_data_reducer';
import disputations from './disputations_reducer';
import auth from './auth_reducer';

export default combineReducers({
    areas, selections, rooms, disputations, auth
});

