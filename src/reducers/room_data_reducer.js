import { FETCH_ROOM_DATA, FETCH_ROOM_DATA_FAILED, FETCH_ROOM_DATA_INITIATED } from "../actions/index";


export default (state={}, action) => {
    
    switch(action.type) {
        
        case FETCH_ROOM_DATA_INITIATED:
            return {...state, [action.payload]: {initiated: true}}
        case FETCH_ROOM_DATA: 
            const roomData = action.payload;
            const equipment = roomData.equipment ? roomData.equipment : [];
            return {...state, [roomData.data.id]: {data: roomData.data, equipment: equipment}}
        case FETCH_ROOM_DATA_FAILED: 
            return {...state, [action.payload]: {...state[action.payload], failed: true}}
        default:
            return state;
    }
}