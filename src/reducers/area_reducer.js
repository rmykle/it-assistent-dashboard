import {GET_AREA_DETAILS} from '../actions'

const defaultState = {0: {shortName: "AHH", longName:"Armauer Hansen Hus", id:0}
,1:{shortName: "BB", longName:"BB-bygget", id: 1}
,2:{shortName: "ODO", longName:"Odontologen", id: 2}};

export default (state=defaultState, action) => {
    
    switch(action.type) {
        case GET_AREA_DETAILS:
        const selectedID = action.payload.id;
        return  {...Object.keys(state)
        .filter( key => key != selectedID)
        .reduce( (res, key) => (res[key] = state[key], res), {} ), [selectedID]: action.payload};
        default:
            return state;
    }
}