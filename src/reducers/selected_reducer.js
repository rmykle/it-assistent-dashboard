import {SELECTED_AREA, SELECTED_EVENT, GET_AREA_DETAILS} from '../actions';

export default (state={selectedDate: new Date()}, action) =>{
    switch(action.type){
        case SELECTED_AREA:
            return {...state, selectedArea: action.payload};
        case SELECTED_EVENT:
            return {...state, selectedEvent: action.payload}
        default:
            return state;
    }
}