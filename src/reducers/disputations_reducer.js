import { FETCH_DISPUTES } from "../actions/index";


export default (state = [], action) => {
    switch(action.type) {
        case FETCH_DISPUTES:
            return action.payload;
        default:
            return state;  
    }
}