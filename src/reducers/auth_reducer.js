import { AUTH_INITIATE, AUTH_FAILED, AUTH_SUCCESS } from "../actions/index";



export default (state={user:"", isAuthed: false, isInitated:false, isFailure: false}, action) => {
    switch(action.type){
        case AUTH_INITIATE:
            return {...state, isInitated: true, isFailure: false};
        case AUTH_FAILED:
            return {...state, isInitated:false, isFailure:true}
        case AUTH_SUCCESS:
        return {...state, isAuthed: true}
        default:
            return state;
    }
}
